
// --------------------------------------------------
const path = require('path');
// -------------------------------------------------


module.exports = {
    mode: 'production' , 
    output : {
        path : path.join(__dirname , 'dist') ,
        filename : 'main.js'
    },
    module : {
        rules : [ 
            {
                test : /\.css$/,
                use : ['style-loader' , 'css-loader']
            },
            {
                test : /\.js|jsx$/,
                use : ['babel-loader']
            }
        ]
    },
    // plugins : [
    //     new htmlWebpackPlugin({
    //         template : path.resolve(__dirname , "dist" , 'index.html'),
    //         filename : "index.html"
    //     }),
    // ],

    devServer : {
        static : './dist/'
    }

}